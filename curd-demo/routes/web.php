<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home/{lang}', function ($lang) {
    return view('home', ['lang'=>$lang]);
});

Route::get('/contact', function () {
    $contacts = array("Ahmed Yossef","ahmed.yossef@nobleprog.ae","00971581059211");
    return view('contacts',['mydata'=> $contacts]);
});