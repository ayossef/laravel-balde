<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
</head>
<body>
    <div class="container">
        <ul>
            <li><a href="/about"> About Me</a></li>
            <li><a href="/contact">Contact Me</a></li>
            <li><a href="/home/{{$lang}}">Home</a></li>
        </ul>
    </div>
        
    @if ($lang == 'ar')
        <div>
            <p>
                <h1>أهلا وسهلا</h1>
            </p>
        </div>
    @else
        <div class="container">
            <p>
                <h1>Welcome to our site</h1>
            </p>
        </div>
    @endif
</body>
</html>